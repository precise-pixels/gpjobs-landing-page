var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    prefix = require('gulp-autoprefixer');

gulp.task('styles', function() {
   return  gulp.src([
       './assets/vendor/bootstrap-sass-official/vendor/assets/stylesheets/bootstrap.scss',
       './assets/sass/**/*.scss'])
       .pipe(sass({output: 'compressed'}))
       .pipe(prefix('last 4 versions'))
       .pipe(gulp.dest('./assets/css'));
});

gulp.task('watch', function() {
    gulp.watch('./assets/sass/**/*.scss', ['styles']);
});


gulp.task('default', ['styles', 'watch']);